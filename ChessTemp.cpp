#include "Chess.h"
#include <iostream>

using std::cout; using std::endl;

///STALEMATE LOGIC
//At the beginnning of make move, see if you are in stalemate 

{

//bool in_stalemate(bool white) const;
//make a list of all the pieces
//

    //Start by copying the board
    std::map<std::pair<char, char>, const Piece *> BCopy = board_copy();
    //Find king loc
    std::pair<char, char> kingLoc = king_loc(white);

    //making a vector of my pieces
    vector<std::pair<char,char>> my_pieces;
    for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {
      //see if the pieces are on your side
      if ((it->second != nullptr) && (it->second->is_white() == white)) {
        my_pieces.push_back(it->first);
      }

    }


    bool valid = false;
    std::pair<char, char> moveTo;


    //go through all of your own pieces and see if they can make valid moves and not be in check
    for (std::vector<std::pair<char, char>> :: iterator it = my_pieces.begin(); it != my_pieces.end(); ++it) {
      //try moving to all parts of the board

      for (int i = 'A'; i < 'A' + 8; i++) {
        for (int j = 1; j < 9; j++) {
          moveTo.first = i;
          moveTo.second = j;
          
          if (legal_capture_shape(it->first, moveTo) && !(in_check(white, it->first, moveTo))) {
            return false;
          }

          if (legal_move_shape(it->first, moveTo) && !(in_check(white, it->first, moveTo))) {
            return false;
          }
        }
      }


    }

    return true;


}

//IN MATE COPIED
bool Chess::in_mate(bool white) const {


  int size = threat.size();
  bool checkingMoves;

  if (size == 1) {
    for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {
    //if anyone in your team can capture the threat
      if ((it->second != nullptr) && (it->second->is_white() == white) 
        && (it->second->legal_capture_shape(it->first, threat.at(0)))) {
        std::cout << "piece: " << it->second->to_ascii() << std::endl;
        std::cout << "here1" << std::endl;
        char pieceType = it->second->to_ascii();
          if ((pieceType == 'R') || (pieceType == 'r')) {
            //check to see if no pieces inbetween
            //returns false if there are pieces in the way
            checkingMoves = checkRook(it->first, threat.at(0));
            if (checkingMoves) {
              //no one in the way, we can eliminate the threat
              //therefore not in checkmate
              return false;
            }
          }

          if ((pieceType == 'Q') || (pieceType == 'q')) {
            //check to see if no pieces inbetween
            checkingMoves = checkQueen(it->first, threat.at(0));
            if (checkingMoves) {
              return false;
            }
          }

          if ((pieceType == 'B') || (pieceType == 'b')) {
            //check to see if no pieces inbetween
            checkingMoves = checkBishop(it->first, threat.at(0));
            if (checkingMoves) {
              return false;
            }
          }
      }
    }
  } else {
    //for any threat
      for (std::vector<std::pair<char, char>> :: iterator it = threat.begin(); it != threat.end(); ++it) {
     
        //can our king take out our threat
        if (BCopy[kingLoc]->legal_capture_shape(kingLoc, *it) && (!in_check(white,kingLoc,*it))) {
          return false;
        }

      }
      //bcopy//you cannot safely elimnate the threat
      return true;
    }

    //PART 2: CAN YOUR KING MOVE OUT OF CHECK 

    vector<std::pair<char,char>> king_moves;

    king_moves.push_back({kingLoc.first + 1, kingLoc.second});
    king_moves.push_back({kingLoc.first - 1, kingLoc.second});
    king_moves.push_back({kingLoc.first, kingLoc.second + 1});
    king_moves.push_back({kingLoc.first, kingLoc.second - 1});
    king_moves.push_back({kingLoc.first + 1, kingLoc.second + 1});
    king_moves.push_back({kingLoc.first + 1, kingLoc.second - 1});
    king_moves.push_back({kingLoc.first - 1, kingLoc.second + 1});
    king_moves.push_back({kingLoc.first - 1, kingLoc.second - 1});


    for (vector<std::pair<char, char>> :: iterator it = king_moves.begin(); it != king_moves.end(); ++it) {
      //If this is even a valid move
      //Try to move your king out of check
      if(BCopy[kingLoc]->legal_move_shape(kingLoc,*it) && BCopy[*it] == nullptr) {
        if (!in_check(white, kingLoc, *it)) { //your king can move out of check
            //therefore not in checkmate
          std::cout << "here2" << std::endl;
          std::cout << it->first << it->second << std::endl;
            return false;
        }
      }
    }


    //PART 3: blocking paths
    //ANALYZE on threat at a time, at this point, we cant elminate threat
    //or move king, so if there are multiple threats, we need to 
    if (size > 1) {
      //You cannot block the checkmate in one move, bc there are multiple threats
      return true;
    }
    else {
      char threatType = BCopy[threat.at(0)]->to_ascii();
      if (threatType == 'r' || threatType == 'R') {
        //return false, you can block the threat and you are not in checkmate
        std::cout << "here3" << std::endl;
        return in_checkMate_Rook(white, threat.at(0), kingLoc, BCopy);
      }

      if (threatType == 'b' || threatType == 'B') {
        std::cout << "here4" << std::endl;
        return in_checkMate_Bishop(white, threat.at(0), kingLoc, BCopy);
      }

      if (threatType == 'q' || threatType == 'Q') {

        //std::cout << "here5" << std::endl;
        //std::cout << "Rook" << in_checkMate_Rook(white, threat.at(0), kingLoc, BCopy) << std::endl;
        //std::cout << "B" << in_checkMate_Bishop(white, threat.at(0), kingLoc, BCopy) << std::endl;
        return (in_checkMate_Rook(white, threat.at(0), kingLoc, BCopy)) && (in_checkMate_Bishop(white, threat.at(0), kingLoc, BCopy));
        //return true;
      }
    }

  //true means you are in check mate
	return true;
}


//Copy constructor
{
    //for any threat
	for (std::vector<std::pair<char, char>> :: iterator it = threat.begin(); it != threat.end(); ++it) {
     
        //can our king take out our threat
        if (BCopy[kingLoc]->legal_capture_shape(kingLoc, *it) && (!in_check(white,kingLoc,*it))) {
        	return false;
        }

	}
	//you cannot safely elimnate the threat
	return true;
}


std::pair<char,char> Chess::king_loc(bool white) const {

  const Piece * tempP;
  std::pair<char, char> temp = {'A', '1'};
  std::pair<char, char> kingLoc = {'A', '1'};
  
  //Finding king 
    for (int i = 0; i < 8; i++) {
      temp.first = 'A' + i;
      temp.second = '1';

      for (int j = 0; j < 8; j++) {
          temp.second = '1' + j;
          tempP = board.operator()(temp);

          if (white) { //looking for white king
            if (tempP->to_ascii() == 'K') {
                kingLoc = temp; 
            }
          } else { //looking for black king
            if (tempP->to_ascii() == 'k') {
                kingLoc = temp; 
            }
          }
      }
    }

    return kingLoc;

}



std::pair<char,char> Chess::king_loc(bool white, std::map<std::pair<char,char>, const Piece *> BCopy) const {

  const Piece * tempP;
  std::pair<char, char> temp = {'A', '1'};
  std::pair<char, char> kingLoc = {'A', '1'};
  
  //Finding king 
    for (int i = 0; i < 8; i++) {
      temp.first = 'A' + i;
      temp.second = '1';

      for (int j = 0; j < 8; j++) {
          temp.second = '1' + j;
          tempP = board.operator()(temp);

          if (white) { //looking for white king
            if (tempP->to_ascii() == 'K') {
                kingLoc = temp; 
            }
          } else { //looking for black king
            if (tempP->to_ascii() == 'k') {
                kingLoc = temp; 
            }
          }
      }
    }

    return kingLoc;

}


//This returns true if you ARE in check by the opponent
bool Chess::in_check(bool white) const {
  
	//Start by copying the board
	std::map<std::pair<char, char>, const Piece *> BCopy;
	const Piece * tempP;

	std::pair<char, char> temp = {'A', '1'};
	std::pair<char, char> kingLoc = {'A', '1'};

	for (int i = 0; i < 8; i++) {
    	temp.first = 'A' + i;
    	temp.second = '1';

    	for (int j = 0; j < 8; j++) {
      		temp.second = '1' + j;
      		tempP = board.operator()(temp);

      		if (tempP != nullptr) {
	      		BCopy[temp] = tempP; 
      		}  

    	}
  	}

  	temp.first = 'A';
  	temp.second = '1';
  
 	//Finding king 
  	for (int i = 0; i < 8; i++) {
    	temp.first = 'A' + i;
    	temp.second = '1';

    	for (int j = 0; j < 8; j++) {
      		temp.second = '1' + j;
      		tempP = board.operator()(temp);

      		if (white) { //looking for white king
        		if (tempP->to_ascii() == 'K') {
          			kingLoc = temp; 
        		}
      		} else { //looking for black king
        		if (tempP->to_ascii() == 'k') {
          			kingLoc = temp; 
        		}
      		}
    	}
  	}

  	for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {

  		//this is the case of testing to see if black pieces can attack white king 
  		if (white && it->second->is_white() != white) {
  			if (it->second->legal_capture_shape(it->first, kingLoc)) {
  				return true;
  			}
  		} 

  		//this is the case of testing to see if white pieces can attack black king
  		if (!white && it->second->is_white() == white) {
  			if (it->second->legal_capture_shape(it->first, kingLoc)) {
  				return true;
  			}
  		} 	

  	}

    return false;
}

bool checkmate(bool white) {

	//if you aren't in check, return false
	if (!in_check(white)) {
		return false;
	}

	//Start by copying the board
	std::map<std::pair<char, char>, const Piece *> BCopy = board_copy();
	const Piece * tempP;

	std::pair<char, char> temp = {'A', '1'};
	std::pair<char, char> kingLoc = king_loc(white);

  	bool check_valid;
  	bool out_of_check = false;

  	// one of the 8 cells king can move to
  	//can it move to the right?

  	#include <vector>
  	vector<std::pair<char,char>> king_moves;

  	king_moves.push_back({kingLoc.first + 1, kingLoc.second});
  	king_moves.push_back({kingLoc.first - 1, kingLoc.second})
  	king_moves.push_back({kingLoc.first, kingLoc.second + 1});
  	king_moves.push_back({kingLoc.first, kingLoc.second - 1});
  	king_moves.push_back({kingLoc.first + 1, kingLoc.second + 1});
  	king_moves.push_back({kingLoc.first + 1, kingLoc.second - 1});
  	king_moves.push_back({kingLoc.first - 1, kingLoc.second + 1});
  	king_moves.push_back({kingLoc.first - 1, kingLoc.second - 1});


  	//to see if it is out of bounds, if so, it would just skip this and go onto the next code block 
  	check_valid = legal_move_shape(kingLoc, temp_king);
  	if (check_valid) {
  		//If move is legal, update the board
  		BCopy[kingLoc] = nullptr;
  		BCopy[temp_king] = board.operator()(temp_king);


  		// for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {

  		// 	//this is the case of testing to see if black pieces can attack white king 
  		// 	if (white && it->second->is_white() != white) {
  		// 		if (it->second->legal_capture_shape(it->first, temp_king)) {
  		// 			out_of_check = true;
  		// 		}	
  		// 	} 

  		// 	//this is the case of testing to see if white pieces can attack black king
  		// 	if (!white && it->second->is_white() == white) {
  		// 		if (it->second->legal_capture_shape(it->first, temp_king)) {
  		// 			out_of_check = true;
  		// 		}
  		// 	} 	
  		// }
  	}

  	if (out_of_check) {
  		return false; // not a checkmate then so function of bool checkmate returns false!
  	}


  	//Then you can do this by updating temp_king
  	// temp_king = {kingLoc.first - 1, kingLoc.second};
  	// temp_king = {kingLoc.first, kingLoc.second + 1};
  	// temp_king = {kingLoc.first, kingLoc.second - 1};
  	// temp_king = {kingLoc.first + 1, kingLoc.second + 1};
  	// temp_king = {kingLoc.first + 1, kingLoc.second - 1};
  	// temp_king = {kingLoc.first - 1, kingLoc.second + 1};
  	// temp_king = {kingLoc.first - 1, kingLoc.second - 1};

  	//You would repeat the same kind of code blocks seen earlier 



  	//make pairs of the 8 cells surrounding the king
  	//if king is in a corner/edge, 

  	// bool checkmate = false;
  	// char pieceType;4

  	// bool out_of_check = false;
  	// bool checking = false;

  	// //make sure these moves are valid first
  	// std::pair<char, char> king1 = {kingLoc.first + 1, kingLoc.second};
  	// checking = legal_capture_shape(kingLoc, king1);
  	// if (checking) {
  	// 	BCopy[king1] = 'K'; //um put in the current king to king1's position 
  	// }

  	// std::map<std::pair<char, char>, const Piece *> BCopy;

  	// if (legal_move_shape(kingLoc, king1)) { 
  	// 	for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {
  	// 		if (white && it->second->is_white() != white) {
  	// 			if (!(it->second->legal_capture_shape(it->first, king1))) {
  	// 				out_of_check = true;
  	// 			}
  	// 		}
  	// 	}

  	// }

  	// if (out_of_check) {
  	// 	return false; // it wouldn't be check
  	// }
  	// std::pair<char, char> king2 = {kingLoc.first - 1, kingLoc.second};
  	// std::pair<char, char> king3 = {kingLoc.first, kingLoc.second + 1};
  	// std::pair<char, char> king4 = {kingLoc.first, kingLoc.second - 1};
  	// std::pair<char, char> king5 = {kingLoc.first + 1, kingLoc.second + 1};
  	// std::pair<char, char> king6 = {kingLoc.first + 1, kingLoc.second - 1};
  	// std::pair<char, char> king7 = {kingLoc.first - 1, kingLoc.second + 1};
  	// std::pair<char, char> king8 = {kingLoc.first - 1, kingLoc.second - 1};

  	// checking = legal_capture_shape(it->first, )

  	// checking = legal_move_shape(kingLoc, king1);
  	// if (checking) {
  	// 	out_of_check = legal_capture_shape()
  	// }

  	// temp_king.first = kingLoc.first + 1;
  	// checking = legal_move_shape(kingLoc, temp_king);


  	// checking = legal_move_shape(kingLoc, )

  	// if (kingLoc.first > 'A') {

  	// }

  	// for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {

  	// 	//this is the case of testing to see if black pieces can attack white king 
  	// 	if (white && it->second->is_white() != white) {
  	// 		if (it->second->legal_capture_shape(it->first, kingLoc)) {
  				
  	// 			pieceType = it->second->to_ascii();

  	// 			if (pieceType == 'P') {

  	// 			}
  	// 			return true;
  	// 		}
  	// 	} 

  	// 	//this is the case of testing to see if white pieces can attack black king
  	// 	if (!white && it->second->is_white() == white) {
  	// 		if (it->second->legal_capture_shape(it->first, kingLoc)) {
  	// 			return true;
  	// 		}
  	// 	} 	

  	// }

  	// for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {

  		// //this is the case of testing to see if black pieces can attack white king 
  		// if (white && it->second->is_white() != white) {

  		// } 

  		// //this is the case of testing to see if white pieces can attack black king
  		// if (!white && it->second->is_white() == white) {

  		// } 	

  	// }

	return checkmate;

}


/**
Notes:
Checkmate logic
1) see if you are in check (if not true, return false)
2) identify the paths from the opponents that can attack 
   your king & mark those squres
3) a) see if your king can move to a location that isn't marked
   b) see if any of your own non-king pieces can land 
      in those squares 
4) If the opponent's piece is a knight, the only way your king 
   avoids death is by is by moving himself (can't block its path)
   OR you find your own piece that can take out the knight

Save is already established in the program
Load doesn't work - so that's what we need to do 

At the beginning of make_move, call in_check(bool white) aka the function in this temporary file  

*/
