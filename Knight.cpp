// Soo Hyun Lee slee387
// Eliza Cohn ecohn4
// Curtis Nishimoto cnishim1
// 601.220, Spring 2019
// Knight.cpp


#include "Knight.h"
#include <iostream>

using std::pair; using std::cout; using std::endl;

bool Knight::legal_move_shape(pair<char,char> start, pair<char, char> end) const {
  if (end.first > ('A' + 7) || end.first < 'A') {
    return false;
  }

  if (end.second > ('1' + 7) || end.second < '1') {
    return false;
  }

  if ((end.first - start.first == 1 || end.first - start.first == -1) &&
      (end.second - start.second == 2 || end.second - start.second == -2)) {
      
    return true;
  }
  if ((end.first - start.first == 2 || end.first - start.first == -2) &&
      (end.second - start.second == 1 || end.second - start.second == -1)) {
    
    return true;
  }
  return false;
}
