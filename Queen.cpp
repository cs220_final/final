// Curtis Nishimoto cnishim1
// Soo Hyun Lee slee387
// Eliza Cohn ecohn4
// 601.220, Spring 2019
// Queen.cpp

#include "Queen.h"
#include <map>

using std::pair;

bool Queen::legal_move_shape(pair<char,char> start, pair<char,char> end) const {

  //check to see if you are moving off the board
  if (end.first > ('A' + 7) || end.first < 'A') {
    return false;
  }
  if (end.second > ('1' + 7) || end.second < '1') {
    return false;
  }

  //horizontal movement
  if (end.first == start.first) {
  	return true;
  }
  
  //vertical movement
  if (end.second == start.second) {
  	return true;
  }

  //diagnoal movement right/top && left/bottom
  if ((end.first - start.first) == (end.second - start.second)) {
  	return true;
  }

  //diagonal movement left/top && right/bottom
  if ((end.first - start.first) == -(end.second - start.second)) {
    return true;
  }

  //does this consider diagonal movement in all directions?
   return false;
 }
