
CONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

chess: main.o Chess.o Board.o Bishop.o CreatePiece.o King.o Knight.o Pawn.o Queen.o Rook.o
	g++ -o chess main.o Chess.o Board.o Bishop.o CreatePiece.o King.o Knight.o Pawn.o Queen.o Rook.o

main.o: main.cpp Chess.h
	g++ -c main.cpp $(CFLAGS)

Chess.o: Chess.cpp Chess.h Piece.h Board.h
	g++ -c Chess.cpp $(CFLAGS)

Board.o: Board.cpp Board.h Piece.h Pawn.h Rook.h Knight.h Bishop.h Queen.h King.h Mystery.h CreatePiece.h Terminal.h
	g++ -c Board.cpp $(CFLAGS)

Bishop.o: Bishop.cpp Bishop.h Piece.h
	g++ -c Bishop.cpp $(CFLAGS)

CreatePiece.o: CreatePiece.cpp CreatePiece.h Piece.h Pawn.h Rook.h Knight.h Bishop.h Queen.h King.h Mystery.h
	g++ -c CreatePiece.cpp $(CFLAGS)

King.o: King.cpp King.h Piece.h
	g++ -c King.cpp $(CFLAGS)

Knight.o: Knight.cpp Knight.h Piece.h
	g++ -c Knight.cpp $(CFLAGS)

Pawn.o: Pawn.cpp Pawn.h Piece.h
	g++ -c Pawn.cpp $(CFLAGS)

Queen.o: Queen.cpp Queen.h Piece.h
	g++ -c Queen.cpp $(CFLAGS)

Rook.o: Rook.cpp Rook.h Piece.h
	g++ -c Rook.cpp $(CFLAGS)

.PHONY: clean
clean:
	rm -f *.o chess *~
