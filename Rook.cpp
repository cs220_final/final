// Curtis Nishimoto cnishim1
// Soo Hyun Lee slee387
// Eliza Cohn ecohn4
// 601.220, Spring 2019
// Rook.cpp

#include "Rook.h"
#include <map>

using std::pair;

bool Rook::legal_move_shape(pair<char,char> start, pair<char,char> end) const {

  //check to see if you are moving off the board
  if (end.first > ('A' + 7) || end.first < 'A') {
    return false;
  }
  if (end.second > ('1' + 7) || end.second < '1') {
    return false;
  }
  
  if (end.first == start.first) {
    return true;
  }
  if (end.second == start.second) {
    return true;
  }

  return false;

}


