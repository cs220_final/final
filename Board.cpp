// Eliza Cohn ecohn4
// Soo Hyun Lee slee387
// Curtis Nishimoto cnishim1
// 601.220, Spring 2019
// Board.cpp

#include <iostream>
#include <string>
#include <utility>
#include <map>
#include "Board.h"
#include "CreatePiece.h"

using std::cout; using std::endl;

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Board::Board(){}

//Destroy all pieces on the board
Board::~Board() {
  for (std::map<std::pair<char, char>, Piece*>::iterator it = occ.begin();
       it != occ.end();
       ++it) {
    delete it->second;
    it->second = nullptr;
  }
}

//Return a piece on the board
const Piece* Board::operator()(std::pair<char, char> position) const {
  if (occ.find(position) != occ.end()) {
    return occ.at(position);
  } 
  return nullptr;
}


//Add a piece on the board
bool Board::add_piece(std::pair<char, char> position, char piece_designator) {
  if (position.first < 'A' || position.first > 'A' + 7) {
    cout << "out of bounds" << endl;
    return false;
  }
  if (position.second < '1' || position.second > '1' + 7) {
    cout << "out of bounds" << endl;
    return false;
  }

  //switch
  switch(piece_designator) {
  case 'B' :
  case 'b' :
  case 'K' :
  case 'k' :
  case 'N' :
  case 'n' :
  case 'P' :
  case 'p' :
  case 'Q' :
  case 'q' :
  case 'R' :
  case 'r' :
  case 'M' :
  case 'm' :
  case '-' :
    occ[position] = create_piece(piece_designator);
    return true;
    break;
  default :
    return false;
    break;
  }
}

//count the kings
bool Board::has_valid_kings() const {
  int kings = 0;
  for (std::map<std::pair<char, char>, Piece*>::const_iterator it = occ.cbegin();
       it != occ.cend();
       ++it) {
    if (it->second != nullptr) {
      if (it->second->to_ascii() == 'k' || it->second->to_ascii() == 'K') {
	kings++;
      }
    }
  }
  return kings == 2;
}

//make the display pretty
std::string to_char(char c) {
  switch(c) {
  case 'B' : return "♗";
  case 'b' : return "♝";
  case 'K' : return "♔";
  case 'k' : return "♚";
  case 'N' : return "♘";
  case 'n' : return "♞";
  case 'P' : return "♙";
  case 'p' : return "♟";
  case 'Q' : return "♕";
  case 'q' : return "♛";
  case 'R' : return "♖";
  case 'r' : return "♜";
  case 'M' : return "M";
  case 'm' : return "m";

  default :
    return " ";
  }

}

//Display the board in a pretty way!
void Board::display() const {

  int count = 1;

  for(char r = '8'; r >= '1'; r--) {
    for(char c = 'A'; c <= 'H'; c++) {
      const Piece* piece = (*this)(std::pair<char, char>(c, r));
      if(count%2 == 0) {
        Terminal::color_bg(Terminal::CYAN);
      }
      else {
        Terminal::color_bg(Terminal::MAGENTA);
      }
      if (piece) {
        Terminal::color_fg(true, Terminal::BLACK);
        cout << to_char(piece->to_ascii());
        cout << " ";      
      } else {
        Terminal::color_fg(true, Terminal::BLACK);
        cout << "  ";
        
      }
      
      count++;
    }
    count++;
    Terminal::set_default();
    cout << std::endl;
  }
  Terminal::set_default();
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator<<(std::ostream& os, const Board& board) {
	for(char r = '8'; r >= '1'; r--) {
		for(char c = 'A'; c <= 'H'; c++) {
			const Piece* piece = board(std::pair<char, char>(c, r));
			if (piece) {
				os << piece->to_ascii();
			} else {
			  os << '-';
			}
		}
		os << std::endl;
	}
	return os;
}

