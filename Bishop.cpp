// Eliza Cohn ecohn4
// Soo Hyun Lee slee387
// Curtis Nishimoto cnishim1
// 601.220, Spring 2019
// Bishop.cpp

#include "Bishop.h"
#include <map>
#include <iostream>

using std::pair; using std::cout; using std::endl;

bool Bishop::legal_move_shape(pair<char,char> start, pair<char,char> end) const {

  //check to see if you are moving off the board
  if (end.first > ('A' + 7) || end.first < 'A') {
    return false;
  }
  if (end.second > ('1' + 7) || end.second < '1') {
    return false;
  }
  
  //go up and to the right
  if (end.first > start.first && end.second > start.second) {
    if (end.first - start.first == end.second - start.second) {
       
      return true;
    }
    return false;
  }

  //go down and to the right
  if (end.second < start.second && end.first > start.first) {
    if (start.second - end.second == end.first - start.first) {
      
      return true;
    }
    return false;
  }

  //go down and to the left
  if (end.second < start.second && end.first < start.first) {
    if (start.second - end.second == start.first - end.first) {
      
      return true;
    }
    return false;
  }

  //go up and to the right
  if (end.second > start.second && end.first > start.first) {
    if (end.second - start.second == end.first - start.first) {
      
      return true;
    }
    return false;
  }

  //go up and to the left
  if (end.second > start.second && end.first < start.first) {
    if (end.second - start.second == start.first - end.first) {
     
      return true;
    }
    return false;
  }
  
  
  //catch all
  return false;

}


