// Soo Hyun Lee slee387
// Eliza Cohn ecohn4
// Curtis Nishimoto cnishim1
// 601.220, Spring 2019
// King.cpp

#include "King.h"

using std::pair;

bool King::legal_move_shape(pair<char, char> start, pair<char, char> end) const {
  if (end.first > ('A' + 7) || end.first < 'A') {
    return false;
  }
  if (end.second > ('1' + 7) || end.second < '1') {
    return false;
  }

  //for readability
  int first = end.first - start.first;
  int second = end.second - start.second; 
  if ((first >= -1 && first <= 1) && (second >= -1 && second <= 1)) {
    return true;
  }

  return false;
}
