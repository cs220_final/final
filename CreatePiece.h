// Eliza Cohn ecohn4
// Soo Hyun Lee slee387
// Curtis Nishimoto cnishim1
// 601.220, Spring 2019
// CreatePiece.h

/////////////////////////////////
// DO NOT MODIFY THIS FILE!!!! //
/////////////////////////////////
#ifndef CREATE_PIECE_H
#define CREATE_PIECE_H

#include "Piece.h"

// This function returns a pointer to a piece of the specified type
// The piece designator should be one of:
//	'K': white king
//	'k': black king
//	'Q': white queen
//	'q': black queen
//	'B': white bishop
//	'b': black bishop
//	'N': white knight
//	'n': black knight
//	'R': white rook
//	'r': black rook
//	'P': white pawn
//	'p': black pawn
//	'M': white mystery piece
//	'm': black mystery piece
Piece* create_piece(char piece_designator);

#endif // CREATE_PIECE_H
