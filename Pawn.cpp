// Soo Hyun Lee slee387
// Eliza Cohn ecohn4
// Curtis Nishimoto cnishim1
// 601.220, Spring 2019
// Pawn.cpp

#include "Pawn.h"
#include <map>

using std::pair;

bool Pawn::legal_move_shape(pair<char,char> start, pair<char,char> end) const {
 
  //check to see if you are moving off the board
  if (end.first > ('A' + 7) || end.first < 'A') {
    return false;
  }
  if (end.second > ('1' + 7) || end.second < '1') {
    return false;
  }
  
  //two types of moves: at start
  if (is_white()) {
    if (end.first == start.first && end.second == (start.second + 1)) {
      return true;
    }
      if (end.first == start.first && end.second == (start.second + 2) && start.second == '2') {
	return true;
      }
  } else {
    if (end.first == start.first && end.second == (start.second - 1)) {
      return true;
    }
      if (end.first == start.first && end.second == (start.second - 2) && start.second == '7') {
	return true;
      }
  }
  
  return false;

}


bool Pawn::legal_capture_shape(pair<char,char> start, pair<char,char> end) const {

//check to see if you are moving off the board
  if (end.first > ('A' + 7) || end.first < 'A') {
    return false;
  }
  if (end.second > ('1' + 7) || end.second < '1') {
    return false;
  }
  if (is_white()) { 
    if (end.second == (start.second + 1) && 
	((end.first == (start.first + 1)) || end.first == (start.first - 1))) {
      return true;
    }
  } else {
    if (end.second == (start.second - 1) && 
	((end.first == (start.first + 1)) || end.first == (start.first - 1))) {
      return true;
    }
  }
  return false;

}
