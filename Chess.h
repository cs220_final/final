// Eliza Cohn ecohn4
// Soo Hyun Lee slee387
// Curtis Nishimoto cnishim1
// 601.220, Spring 2019
// Chess.h

#ifndef CHESS_H
#define CHESS_H

#include <iostream>
#include "Piece.h"
#include "Board.h"

class Chess {

public:
	// This default constructor initializes a board with the standard
	// piece positions, and sets the state to white's turn
	Chess();

	// Returns a constant reference to the board 
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	const Board& get_board() const { return board; }

	// Returns true if it is white's turn
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
        bool turn_white() const { return is_white_turn; }
        // Returns a reference to the board
        Board& board_get() { return board; }
        // Function to set whose turn it is
        void set_white(bool b) { is_white_turn = b; }
	// Attemps to make a move. If successful, the move is made and
	// the turn is switched white <-> black
	bool make_move(std::pair<char, char> start, std::pair<char, char> end);

	// Returns true if the designated player is in check
	bool in_check(bool white, std::pair<char, char> start, std::pair<char, char> end) const;

        bool in_check(bool white) const;
        
	// Returns true if the designated player is in mate
	std::map<std::pair<char,char>, const Piece *> board_copy() const;

	std::pair<char,char> king_loc(bool white) const;

	std::pair<char,char> king_loc_bcopy(bool white, std::map<std::pair<char,char>, const Piece * > BCopy) const;

	bool in_checkMate_Rook(bool white, std::pair<char, char> threat, std::pair<char, char> kingLoc, 
		std::map<std::pair<char,char>, const Piece *> BCopy) const;

	bool in_checkMate_Bishop(bool white, std::pair<char, char> threat, std::pair<char, char> kingLoc, 
		std::map<std::pair<char,char>, const Piece *> BCopy) const;

	bool in_mate(bool white) const;

	// Returns true if the designated player is in mate
	bool in_stalemate(bool white) const;
  
private:
	// The board
	Board board;

	// Is it white's turn?
	bool is_white_turn;

	// Checking if there are existing pieces between start and end (rook)
  bool checkRook(std::pair<char, char> start, std::pair<char, char> end, std::map<std::pair<char,char>, const Piece * > BCopy) const;

	// Checking if there are existing pieces between start and end (queen)
  bool checkQueen(std::pair<char, char> start, std::pair<char, char> end, std::map<std::pair<char,char>, const Piece * > BCopy) const;

	// Checking if there are existing pieces between start and end (bishop)
  bool checkBishop(std::pair<char, char> start, std::pair<char, char> end, std::map<std::pair<char,char>, const Piece * > BCopy) const;

  bool checkPawn(std::pair<char, char> start,std::map<std::pair<char,char>, const Piece * > BCopy ) const;
	// Make a deep copy
	void updateBoardCopy(std::pair<char, char> start, std::pair<char, char> end);
};

// Writes the board out to a stream
std::ostream& operator<< (std::ostream& os, const Chess& chess);

// Reads the board in from a stream
std::istream& operator>> (std::istream& is, Chess& chess);


#endif // CHESS_H
