///////////////////////////////////
// IT IS OK TO MODIFY THIS FILE, //
// YOU WON'T HAND IT IN!!!!!     //
///////////////////////////////////
#ifndef MYSTERY_H
#define MYSTERY_H

#include "Piece.h"

class Mystery : public Piece {

public:
  // Moves in 3x2 pattern
	bool legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  //check to see if you are moving off the board
  		if (end.first > ('A' + 7) || end.first < 'A') {
    		return false;
  		}

  		if (end.second > ('1' + 7) || end.second < '1') {
    		return false;
  		}

  		//horizontal movement
  		if (end.first == start.first) {
  			return true;
  		}
  
  		//vertical movement
  		if (end.second == start.second) {
  			return true;
  		}	

  		//diagnoal movement right/top && left/bottom
  		if ((end.first - start.first) == (end.second - start.second)) {
  			return true;
  		}

  		//diagonal movement left/top && right/bottom
  		if ((end.first - start.first) == -(end.second - start.second)) {
    		return true;
  		}

  		if ((end.first - start.first == 3 || end.first - start.first == -3) &&
      		(end.second - start.second == 2 || end.second - start.second == -2)) {
      
    		return true;
  		}
  		
  		if ((end.first - start.first == 2 || end.first - start.first == -2) &&
      		(end.second - start.second == 3 || end.second - start.second == -3)) {
    
    		return true;
  		}

  		//does this consider diagonal movement in all directions?
   		return false;
	}



	char to_ascii() const {
		return is_white() ? 'M' : 'm';
	}

private:
	Mystery(bool is_white) : Piece(is_white) {}

	friend Piece* create_piece(char piece_designator);
};

#endif // MYSTERY_H
