// Eliza Cohn ecohn4
// Soo Hyun Lee slee387
// Curtis Nishimoto cnishim1
// 601.220, Spring 2019
// Chess.cpp

#include "Chess.h"
#include <vector>
#include <iostream>
#include <utility>

using std::endl; using std::vector;
using std::make_pair; using std::cerr;


/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Chess::Chess() : is_white_turn(true) {
	// Add the pawns
	for (int i = 0; i < 8; i++) {
		board.add_piece(std::pair<char, char>('A' + i, '1' + 1), 'P');
		board.add_piece(std::pair<char, char>('A' + i, '1' + 6), 'p');
	}

	// Add the rooks
	board.add_piece(std::pair<char, char>( 'A'+0 , '1'+0 ) , 'R' );
	board.add_piece(std::pair<char, char>( 'A'+7 , '1'+0 ) , 'R' );
	board.add_piece(std::pair<char, char>( 'A'+0 , '1'+7 ) , 'r' );
	board.add_piece(std::pair<char, char>( 'A'+7 , '1'+7 ) , 'r' );

	// Add the knights
	board.add_piece(std::pair<char, char>( 'A'+1 , '1'+0 ) , 'N' );
	board.add_piece(std::pair<char, char>( 'A'+6 , '1'+0 ) , 'N' );
	board.add_piece(std::pair<char, char>( 'A'+1 , '1'+7 ) , 'n' );
	board.add_piece(std::pair<char, char>( 'A'+6 , '1'+7 ) , 'n' );

	// Add the bishops
	board.add_piece(std::pair<char, char>( 'A'+2 , '1'+0 ) , 'B' );
	board.add_piece(std::pair<char, char>( 'A'+5 , '1'+0 ) , 'B' );
	board.add_piece(std::pair<char, char>( 'A'+2 , '1'+7 ) , 'b' );
	board.add_piece(std::pair<char, char>( 'A'+5 , '1'+7 ) , 'b' );

	// Add the kings and queens
	board.add_piece(std::pair<char, char>( 'A'+3 , '1'+0 ) , 'Q' );
	board.add_piece(std::pair<char, char>( 'A'+4 , '1'+0 ) , 'K' );
	board.add_piece(std::pair<char, char>( 'A'+3 , '1'+7 ) , 'q' );
	board.add_piece(std::pair<char, char>( 'A'+4 , '1'+7 ) , 'k' );
}

bool Chess::make_move(std::pair<char, char> start, std::pair<char, char> end) {

  //check to see if moving
  if ((start.first == end.first) && (start.second == end.second)) {

    cerr << "Same Place" << endl;
    return false;
    
   } 

    std::map<std::pair<char,char>, const Piece *> BCopy = board_copy();
  
    //get the pieces on the board you are interacting with 
    const Piece* startP = board.operator()(start);
    const Piece* endP = board.operator()(end);

    if (startP == nullptr) {
      cerr << "Nothing Here" << endl;
      return false;
    }

    //check if moving your own piece
    //if it is whites turn
    if (is_white_turn) {
      //and your piece is black
      if(!startP->is_white()){
	      cerr << "Can't move opponents" << endl;
        return false;
      }
    }
    //else, it is blacks turn
    else {
      //check if piece is white
      if (startP->is_white()){
	      cerr << "Can't move opponents" << endl;
        return false;
      }
    }
    
    
    bool moving = false; //capturing
    if (endP == nullptr) {
      moving = true;
    }

    //check to see if you are capturing your own piece
    bool makeMove;
    if (!moving) {
      makeMove = startP->legal_capture_shape(start,end);
      if (endP->is_white() == is_white_turn) {
	      std::cerr << "Capturing Own Piece" << std::endl;
	      return false;
      }
    } else {
       makeMove = startP->legal_move_shape(start,end);
    }
    //valid move shape?
    if (!makeMove) { //invalid move
      std::cerr << "Invalid Move" << std::endl;
      return false;
    } 

    //check to see if you are moving through pieces
    bool checkingMoves = true;
    char pieceType = startP->to_ascii();
    if ((pieceType == 'R') || (pieceType == 'r')) {
      //check to see if no pieces inbetween
      checkingMoves = checkRook(start, end,BCopy);
      if (!checkingMoves) {
	      cerr << "Invalid move" << endl;
        return false;
      }
    }

    if ((pieceType == 'Q') || (pieceType == 'q')) {
      //check to see if no pieces inbetween
      checkingMoves = checkQueen(start, end,BCopy);
      if (!checkingMoves) {
        cerr << "Invalid move" << endl;
	      return false;
      }
    }

    if ((pieceType == 'M') || (pieceType == 'm')) {
      //check to see if no pieces inbetween
      checkingMoves = checkQueen(start, end,BCopy);
      if (!checkingMoves) {
        cerr << "Invalid move" << endl;
        return false;
      }
    }
    
    if ((pieceType == 'B') || (pieceType == 'b')) {
      //check to see if no pieces inbetween
      checkingMoves = checkBishop(start, end,BCopy);
      if (!checkingMoves) {
        cerr << "Invalid move" << endl;
	      return false;
      }
    }

    if ((pieceType == 'P' || (pieceType == 'p'))) {
      if (start.second == '2' || start.second == '7') {
        checkingMoves = checkPawn(start,BCopy);
        if (!checkingMoves) {
          cerr << "Invalid move" << endl;
          return false;
        }
      }
    }


    //update the board with the move!
    bool addMove = true;

    //adding the pawn --> queen factor (white)
    if ((pieceType == 'P') && makeMove) {
      if (end.second == '8') {
        addMove = board.add_piece(end,'Q');
        if (!addMove) {
          cerr << "Invalid move" << endl;
          return false;
        }
	      board.add_piece(start,'-');
	      is_white_turn = !is_white_turn;
        return true;
      }
    }

    //adding the pawn --> queen factor (black)
    if ((pieceType == 'p') && makeMove) {
      if (end.second == '1') {
        addMove = board.add_piece(end,'q');
        if (!addMove) {
          cerr << "Invalid move" << endl;
          return false;
        }
	      board.add_piece(start,'-');
	      is_white_turn = !is_white_turn;
        return true;
      }
    }
    
    //If you are in check, you must make a move to put youself out of check
    //Check to see if the move you are about to make putsyourself out of check
    bool checkMove = false;
    checkMove = in_check(is_white_turn,start,end);
    if (checkMove) {
      cerr << "You can't make this move, you are putting yourself in check!" << endl;
      return false;
    }
    
    //if this is a safe move to make, make it
    addMove = board.add_piece(end,startP->to_ascii());
    if (!addMove) {
      cerr << "Invalid move" << endl;
      return false;
    }
   
    addMove = board.add_piece(start,'-');
    if (!addMove) {
      cerr << "Invalid move" << endl;
      return false;
    }
 
    is_white_turn = !is_white_turn;
    //sucessful move
    return true;
}

//Check to see if the pawn is moving through pieces
bool Chess::checkPawn(std::pair<char, char> start, std::map<std::pair<char,char>, const Piece *> BCopy) const {
  
  const Piece* checkP;
  std::pair<char,char> temp = {start.first,start.second};

  //white team
  if (start.second == '2') {
    temp.second = '3';
    checkP = BCopy[temp];
    if (checkP != nullptr) {
          return false;
    }
  }

  //black team
  if (start.second == '7') {
    temp.second = '6';
    checkP = BCopy[temp];
    if (checkP != nullptr) {
          return false;
    }
  }

  return true;

}

//Check to see if the bishop is moving through pieces
bool Chess::checkBishop(std::pair<char, char> start, std::pair<char, char> end, std::map<std::pair<char,char>, const Piece *> BCopy ) const {

  const Piece* checkP;
  std::pair<char,char> temp = {start.first,start.second};
  
  //diagnoal movement right/top
  if ((end.first > start.first) && (end.second > start.second)) {
      for (int i = 1; i < end.first - start.first; i++) {
        temp.first = start.first + i;
        temp.second = start.second + i;
        checkP = BCopy[temp];

        if (checkP != nullptr) {
          return false;
        }
      }
  }
  //diagonal movement right/bottom
  if ((end.first > start.first) && (end.second < start.second)) {
    for (int i = 1; i < end.first - start.first; i++) {
      temp.first = start.first + i;
      temp.second = start.second - i;
        checkP = BCopy[temp];
	
        if (checkP != nullptr) {
          return false;
        }
    }
  }

  //diagonal movement left/top
  if ((end.first < start.first) && (end.second > start.second)) {
    for (int i = 1; i < start.first - end.first; i++) {
      temp.first = start.first - i;
      temp.second = start.second + i;
      checkP = BCopy[temp];
      
      if (checkP != nullptr) {
	return false;
      }
    }
  }

  //diagonal movement left/bottom
  if ((end.first < start.first) && (end.second < start.second)) {
    for (int i = 1; i < start.first - end.first; i++) {
      temp.first = start.first - i;
      temp.second = start.second - i;
      checkP = BCopy[temp];
      
      if (checkP != nullptr) {
	return false;
      }
    }
  }
  
  return true;
}

//Check to see if the queen is moving through pieces
bool Chess::checkQueen(std::pair<char, char> start, std::pair<char, char> end, std::map<std::pair<char,char>, const Piece *> BCopy) const {

  //check diagonal movement
  bool bishop = checkBishop(start,end, BCopy);
  //check hortizontal/vertical movement
  bool rook = checkRook(start,end, BCopy);

  //if nothing in the way, these are both true
  if (bishop && rook) {
    return true;
  }
  //else, one returned false and there is in something in the way
  return false;
}

//Check to see if the rook is moving through pieces
bool Chess::checkRook(std::pair<char, char> start, std::pair<char, char> end, std::map<std::pair<char,char>, const Piece *> BCopy) const {

  const Piece* checkP;
  std::pair<char,char> temp = {start.first,start.second};
  
  //moving vertically
  if (start.first == end.first) {
    if (start.second < end.second) {
      for (int i = (start.second + 1); i < end.second; i++) {
        //make pair of start.first and i
        temp.second = i;
        checkP = BCopy[temp];
        if (checkP != nullptr) {
	        return false;
        }
      }
    }
    else {
      for (int i = (end.second + 1); i < start.second; i++) {
        //make pair of start.first and i
        temp.second = i;
        checkP = BCopy[temp];
        if (checkP != nullptr) {
	        return false;
        }
      }
    }
  }

  temp = {start.first,start.second};
  //moving horizontally
  if (start.second == end.second) {
    if (start.first < end.first) {
      for (int i = (start.first + 1); i < end.first; i++) {
        //make pair of start.first and i
        temp.first = i;
        checkP = BCopy[temp];
        if (checkP != nullptr) {
      	  return false;
        }
      }
    }
    else {
      for (int i = (end.first + 1); i < start.first; i++) {
        //make pair of start.first and i
        temp.first = i;
        checkP = BCopy[temp];
        if (checkP != nullptr) {
      	  return false;
        }
      }
    }
  }


  //catch all
  return true;
}

//make a copy of the current board
//return a map of the pieces and their locations
std::map<std::pair<char,char>, const Piece *> Chess::board_copy() const {
  std::map<std::pair<char, char>, const Piece *> BCopy;
  const Piece* tempP;
  std::pair<char, char> temp = {'A', '1'};
  

  for (int i = 0; i < 8; i++) {
      temp.first = 'A' + i;
      temp.second = '1';

      for (int j = 0; j < 8; j++) {
          temp.second = '1' + j;
          tempP = board.operator()(temp);

          if (tempP != nullptr) {
            BCopy[temp] = tempP; 
          }  

      }
    }

  return BCopy;
}

//find where the king is on the board
std::pair<char,char> Chess::king_loc(bool white) const {

  const Piece * tempP;
  std::pair<char, char> temp = {'A', '1'};
  std::pair<char, char> kingLoc = {'A', '1'};
  
  //Finding king 
    for (int i = 0; i < 8; i++) {
      temp.first = 'A' + i;
      temp.second = '1';

      for (int j = 0; j < 8; j++) {
          temp.second = '1' + j;
          tempP = board.operator()(temp);

          if (white) { //looking for white king
            if (tempP != nullptr && tempP->to_ascii() == 'K') {
                kingLoc = temp; 
            }
          } else { //looking for black king
            if (tempP != nullptr && tempP->to_ascii() == 'k') {
                kingLoc = temp; 
            }
          }
      }
    }

    return kingLoc;

}

//looking for the king on a copy of the board, not the actual board
//allows one to look for the king on theoretical boards
std::pair<char,char> Chess::king_loc_bcopy(bool white, std::map<std::pair<char,char>, const Piece * > BCopy) const {

  const Piece * tempP;
  std::pair<char, char> temp = {'A', '1'};
  std::pair<char, char> kingLoc = {'A', '1'};
  
  //Finding king 
    for (int i = 0; i < 8; i++) {
      temp.first = 'A' + i;
      temp.second = '1';

      for (int j = 0; j < 8; j++) {
          temp.second = '1' + j;
          tempP = BCopy[temp];

          if (white) { //looking for white king
            if (tempP != nullptr && tempP->to_ascii() == 'K') {
                kingLoc = temp; 
            }
          } 
          else { //looking for black king
            if (tempP != nullptr && tempP->to_ascii() == 'k') {
                kingLoc = temp; 
            }
          }
      }
    }

    return kingLoc;

}

//If the board is in a check state, notify the player
//@param: white indicates who's turn it currently is
//So if white is true, we are checking if white is in check
bool Chess::in_check(bool white) const {
  
  const Piece* tempP;
  std::pair<char,char> temp = {'A','1'};
  bool checkingMoves = true;

  //Finding king
  std::pair<char,char> kingLoc = king_loc(white);

  std::map<std::pair<char,char>, const Piece *> BCopy = board_copy();
  temp.first = 'A';
  temp.second = '1';
  
  //Can any of the opposing pieces capture your king?
  for (int i = 0; i < 8; i++) {
    temp.first = 'A' + i;
    temp.second = '1';
    
    for (int j = 0; j < 8; j++) {
      temp.second = '1' + j;
      tempP = board.operator()(temp);
      if (tempP != nullptr) {
	      //On the opposing team
	      if (tempP->is_white() != white) {
	        //Can we legally capture the king
	        if (tempP->legal_capture_shape(temp, kingLoc)) {
	          char pieceType = tempP->to_ascii();

              //make sure you are not moving through pieces
              if ((pieceType == 'R') || (pieceType == 'r')) {
                //check to see if no pieces inbetween
                checkingMoves = checkRook(temp, kingLoc,BCopy);
                  if (!checkingMoves) {
	                  return false;
                  }
              }

              if ((pieceType == 'Q') || (pieceType == 'q')) {
                //check to see if no pieces inbetween
                checkingMoves = checkQueen(temp, kingLoc,BCopy);
                if (!checkingMoves) {
	                return false;
                }
              }

              if ((pieceType == 'B') || (pieceType == 'b')) {
                //check to see if no pieces inbetween
		checkingMoves = checkBishop(temp, kingLoc,BCopy);
                if (!checkingMoves) {
	                return false;
                }
              }

              if ((pieceType == 'P' || (pieceType == 'p'))) {
                if (temp.second == '2' || temp.second == '7') {
                  checkingMoves = checkPawn(temp,BCopy);
                  if (!checkingMoves) {
                    return false;
                  }
                }
              }
              if ((pieceType == 'M') || (pieceType == 'm')) {
                //check to see if no pieces inbetween
                //check to see if trying to move like a queen 
                checkingMoves = checkQueen(temp, kingLoc,BCopy);
                if (!checkingMoves) {
                  return false;
                }
              }
	            //you can capture the king, and no pieces in between
	            return true;
	          }
	        }
        }
      }
    }

  //Else you are not in check
  return false;
}

/*
 * @param: white is who's turn it is
 * @param: start and end are the proposed moves
 * allows you to check if a proposed move puts you in check
 */ 
bool Chess::in_check(bool white, std::pair<char, char> start, std::pair<char, char> end) const {
  
  std::map<std::pair<char,char>, const Piece*> BCopy = board_copy();
 
  
  BCopy[end] = BCopy[start];
  BCopy[start] = nullptr;
  
  std::pair<char,char> kingLoc = king_loc_bcopy(white, BCopy);
  bool checkingMoves = true;

  for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {
    //if any of the opposing team can capture your king
    if ((it->second != nullptr) && (it->second->is_white() != white) 
      && (it->second->legal_capture_shape(it->first, kingLoc))) {

        char pieceType = it->second->to_ascii();

          //make sure you are not moving through pieces
          if ((pieceType == 'R') || (pieceType == 'r')) {
            //check to see if no pieces inbetween
            checkingMoves = checkRook(it->first, kingLoc,BCopy);
            if (!checkingMoves) {
            
	             return false;
            }
          }
	  
          if ((pieceType == 'Q') || (pieceType == 'q') || (pieceType == 'M') || (pieceType == 'm')) {
            //check to see if no pieces inbetween
            checkingMoves = checkQueen(it->first, kingLoc,BCopy);
            if (!checkingMoves) {
              return false;
            }
          }
	  
          if ((pieceType == 'B') || (pieceType == 'b')) {
            //check to see if no pieces inbetween
            checkingMoves = checkBishop(it->first, kingLoc,BCopy);
            if (!checkingMoves) {
              return false;
            }
          }
	  
          if ((pieceType == 'P' || (pieceType == 'p'))) {
            if (start.second == '2' || start.second == '7') {
              checkingMoves = checkPawn(start,BCopy);
              if (!checkingMoves) {
                return false;
              }
            }
          }

          if ((pieceType == 'M') || (pieceType == 'm')) {
                //check to see if no pieces inbetween
                //check to see if trying to move like a queen 
	    checkingMoves = checkQueen(it->first, kingLoc,BCopy);
                if (!checkingMoves) {
                  return false;
                }
          }

	        return true;
    }
  }
  return false;
}


bool Chess::in_mate(bool white) const {
  //if you aren't in check, return false
    if (!in_check(white)) {
        return false;
    }

    //Start by copying the board
    std::map<std::pair<char, char>, const Piece *> BCopy = board_copy();
    //Find king loc
    std::pair<char, char> kingLoc = king_loc(white);


    //PART 1: CAN YOU TAKE THE OPPONENT OUT

    //Finding threats!
    vector<std::pair<char,char>> threat;
    for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {
    //if any of the opposing team can capture your king
    if ((it->second != nullptr) && (it->second->is_white() != white) 
      && (it->second->legal_capture_shape(it->first, kingLoc))) {

      threat.push_back(it->first);

    }
  }

  int size = threat.size();
  bool checkingMoves;

  if (size == 1) {
    for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {
    //if anyone in your team can capture the threat
      if ((it->second != nullptr) && (it->second->is_white() == white) 
        && (it->second->legal_capture_shape(it->first, threat.at(0)))) {

        char pieceType = it->second->to_ascii();

          //make sure you are not moving through pieces!
          if ((pieceType == 'R') || (pieceType == 'r')) {
            //check to see if no pieces inbetween
            //returns false if there are pieces in the way
            checkingMoves = checkRook(it->first, threat.at(0),BCopy);
            if (checkingMoves) {
              //no one in the way, we can eliminate the threat
              //therefore not in checkmate
              return false;
            }
          }

          if ((pieceType == 'Q') || (pieceType == 'q')) {
            //check to see if no pieces inbetween
            checkingMoves = checkQueen(it->first, threat.at(0),BCopy);
            if (checkingMoves) {
              return false;
            }
          }

          if ((pieceType == 'B') || (pieceType == 'b')) {
            //check to see if no pieces inbetween
            checkingMoves = checkBishop(it->first, threat.at(0),BCopy);
            if (checkingMoves) {
              return false;
            }
          }

          if ((pieceType == 'M') || (pieceType == 'm')) {
                //check to see if no pieces inbetween
                //check to see if trying to move like a queen 
	    checkingMoves = checkQueen(it->first, threat.at(0),BCopy);
                if (checkingMoves) {
                  return false;
                }
          }
      }
    }
  } else {
    //for any threat
      for (std::vector<std::pair<char, char>> :: iterator it = threat.begin(); it != threat.end(); ++it) {
     
        //can our king take out our threat
        if (BCopy[kingLoc]->legal_capture_shape(kingLoc, *it) && (!in_check(white,kingLoc,*it))) {
          return false;
        }

      }
      //if your king cant move to take out a threat
      //then you cannot safely eliminate the threat
      //and you are in check mate
      return true;
    }

    //PART 2: CAN YOUR KING MOVE OUT OF CHECK 

    vector<std::pair<char,char>> king_moves;

    king_moves.push_back({kingLoc.first + 1, kingLoc.second});
    king_moves.push_back({kingLoc.first - 1, kingLoc.second});
    king_moves.push_back({kingLoc.first, kingLoc.second + 1});
    king_moves.push_back({kingLoc.first, kingLoc.second - 1});
    king_moves.push_back({kingLoc.first + 1, kingLoc.second + 1});
    king_moves.push_back({kingLoc.first + 1, kingLoc.second - 1});
    king_moves.push_back({kingLoc.first - 1, kingLoc.second + 1});
    king_moves.push_back({kingLoc.first - 1, kingLoc.second - 1});


    for (vector<std::pair<char, char>> :: iterator it = king_moves.begin(); it != king_moves.end(); ++it) {
      //If this is even a valid move
      //Try to move your king out of check
      if(BCopy[kingLoc]->legal_move_shape(kingLoc,*it) && BCopy[*it] == nullptr) {
        if (!in_check(white, kingLoc, *it)) { //your king can move out of check
            //therefore not in checkmate
            return false;
        }
      }
    }


    //PART 3: blocking paths
    //Analyze one threat at a time
    if (size > 1) {
      //You cannot block the checkmate in one move
      //because there are multiple threats
      return true;
    }
    else {
      //there is only one threat, can you make a move to block it?
      char threatType = BCopy[threat.at(0)]->to_ascii();

      //check if the threat needs to move through pieces
      if (threatType == 'r' || threatType == 'R') {
        //return false, you can block the threat and you are not in checkmate
        return in_checkMate_Rook(white, threat.at(0), kingLoc, BCopy);
      }

      if (threatType == 'b' || threatType == 'B') {
        return in_checkMate_Bishop(white, threat.at(0), kingLoc, BCopy);
      }

      if (threatType == 'q' || threatType == 'Q') {
        
        //combination of rook and bishop
        return (in_checkMate_Rook(white, threat.at(0), kingLoc, BCopy)) && (in_checkMate_Bishop(white, threat.at(0), kingLoc, BCopy));
        
      }

      if (threatType == 'M' || threatType == 'm') {
       
         return (in_checkMate_Rook(white, threat.at(0), kingLoc, BCopy)) && (in_checkMate_Bishop(white, threat.at(0), kingLoc, BCopy));
        
      }
    }

  //true means you are in check mate
	return true;
}

//given the location of the threat and your king
//can any of your pieces block to move the threat
bool Chess::in_checkMate_Rook(bool white, std::pair<char, char> threat, 
    std::pair<char, char> kingLoc, std::map<std::pair<char,char>, const Piece *> BCopy) const {

  std::pair<char,char> temp = {threat.first,threat.second};
  //START = THREAT, END = KINGLOC

  //if a rook
  //moving vertically
  if (threat.first == kingLoc.first) {
    if (threat.second < kingLoc.second) {
      for (int i = (threat.second + 1); i < kingLoc.second; i++) {

        temp.second = i;
        for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {
          //if anyone in your team can capture the threat

          if ((it->second != nullptr) && (it->second->to_ascii() != 'k') 
            && (it->second->to_ascii() != 'K') && (it->second->is_white() == white) 
            && (it->second->legal_move_shape(it->first, temp))) {
               //you can block the threat!
               
               return false;
           }
        }
      }
    }
    else {
      for (int i = (kingLoc.second + 1); i < threat.second; i++) {
       
        temp.second = i;
        for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {
          //if anyone in your team can capture the threat
          if ((it->second != nullptr) && (it->second->to_ascii() != 'k') 
            && (it->second->to_ascii() != 'K') && (it->second->is_white() == white) 
            && (it->second->legal_move_shape(it->first, temp))) {
               //you can block the threat!
              
               return false;
           }
        }
      }
    }
  }

  //moving horizontally
  temp = {threat.first,threat.second};
  if (threat.second == kingLoc.second) {
    if (threat.first < kingLoc.first) {
      for (int i = (threat.first + 1); i < kingLoc.first; i++) {
        //make pair of start.first and i
        temp.first = i;
        for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {
          //if anyone in your team can capture the threat
          if ((it->second != nullptr) && (it->second->to_ascii() != 'k') 
            && (it->second->to_ascii() != 'K') && (it->second->is_white() == white) 
            && (it->second->legal_move_shape(it->first, temp))) {
               //you can block the threat!
              
               return false;
           }
        }
      }
    }
    else {
      for (int i = (kingLoc.first + 1); i < threat.first; i++) {
        //make pair of start.first and i
        temp.first = i;
        for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {
          //if anyone in your team can capture the threat
          if ((it->second != nullptr) && (it->second->to_ascii() != 'k') 
            && (it->second->to_ascii() != 'K') && (it->second->is_white() == white) 
            && (it->second->legal_move_shape(it->first, temp))) {
               //you can block the threat!
               
               return false;
           }
        }
      }
    }
  }
  
  //you cannot block the threat
  return true;
}

//given the location of the threat and your king
//can any of your pieces block to move the threat
bool Chess::in_checkMate_Bishop(bool white, std::pair<char, char> threat, 
    std::pair<char, char> kingLoc, std::map<std::pair<char,char>, const Piece *> BCopy) const {

    std::pair<char,char> temp = {threat.first,threat.second};
    
    //diagnoal movement right/top
  if ((kingLoc.first > threat.first) && (kingLoc.second > threat.second)) {
      for (int i = 1; i < kingLoc.first - threat.first; i++) {
        temp.first = threat.first + i;
        temp.second = threat.second + i;
        for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {
          //if anyone in your team can capture the threat
          if ((it->second != nullptr) && (it->second->to_ascii() != 'k') 
            && (it->second->to_ascii() != 'K') && (it->second->is_white() == white) 
            && (it->second->legal_move_shape(it->first, temp))) {
               //you can block the threat!

               return false;
           }
        }
      }
  }
  //diagonal movement right/bottom
  if ((kingLoc.first > threat.first) && (kingLoc.second < threat.second)) {
    for (int i = 1; i < kingLoc.first - threat.first; i++) {
      temp.first = threat.first + i;
      temp.second = threat.second - i;
      for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {
          //if anyone in your team can capture the threat
          if ((it->second != nullptr) && (it->second->to_ascii() != 'k') 
            && (it->second->to_ascii() != 'K') && (it->second->is_white() == white) 
            && (it->second->legal_move_shape(it->first, temp))) {
               //you can block the threat!
               return false;
           }
        }
    }
  }

  //diagonal movement left/top
  if ((kingLoc.first < threat.first) && (kingLoc.second > threat.second)) {
    for (int i = 1; i < threat.first - kingLoc.first; i++) {
      temp.first = threat.first - i;
      temp.second = threat.second + i;
      for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {
          //if anyone in your team can capture the threat
          if ((it->second != nullptr) && (it->second->to_ascii() != 'k') 
            && (it->second->to_ascii() != 'K') && (it->second->is_white() == white) 
            && (it->second->legal_move_shape(it->first, temp))) {
               //you can block the threat!
               return false;
           }
        }
    }
  }

  //diagonal movement left/bottom
  if ((kingLoc.first < threat.first) && (kingLoc.second < threat.second)) {
    for (int i = 1; i < threat.first - kingLoc.first; i++) {
      temp.first = threat.first - i;
      temp.second = threat.second - i;
      for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {
          //if anyone in your team can capture the threat
          if ((it->second != nullptr) && (it->second->to_ascii() != 'k') 
            && (it->second->to_ascii() != 'K') && (it->second->is_white() == white) 
            && (it->second->legal_move_shape(it->first, temp))) {
               //you can block the threat!
               return false;
           }
        }
    }
  }
  
  return true;

}


//analyze if the given board is a stalemate
//this means none of your peices can move without getting placed in check
bool Chess::in_stalemate(bool white) const {

    //Start by copying the board
    std::map<std::pair<char, char>, const Piece *> BCopy = board_copy();

    //making a vector of my pieces
    std::map<std::pair<char, char>, const Piece *> my_pieces;
    for (std::map<std::pair<char, char>, const Piece *> :: const_iterator it = BCopy.cbegin(); it != BCopy.cend(); ++it) {
      //see if the pieces are on your side
      if ((it->second != nullptr) && (it->second->is_white() == white)) {
        my_pieces[it->first] = it->second;
      }

    }

    std::pair<char, char> moveTo;


    //go through all of your own pieces and see if they can make valid moves and not be in check
    for (std::map<std::pair<char, char>, const Piece *> :: iterator it = my_pieces.begin(); it != my_pieces.end(); ++it) {
      //try moving to all parts of the board

      for (int i = 'A'; i < 'A' + 8; i++) {
        for (int j = '1'; j < '1' + 8; j++) {
          moveTo.first = i;
          moveTo.second = j;

          //if someone is there, it is a capture
          if ((BCopy[moveTo] != nullptr)) {
            if ((BCopy[moveTo]->is_white() != white) && it->second->legal_capture_shape(it->first, moveTo)) {
              if (!in_check(white, it->first, moveTo)) {
                return false;
              }
            }

          } 
          //if no one is there, it is a move
          else {
            if (it->second->legal_move_shape(it->first, moveTo) && !(in_check(white, it->first, moveTo))) {
              return false;
            }
          }

        }
      }


    }

    return true;

}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator<< (std::ostream& os, const Chess& chess) {
	// Write the board out and then either the character 'w' or the character 'b',
	// depending on whose turn it is
	return os << chess.get_board() << (chess.turn_white() ? 'w' : 'b');
}


std::istream& operator>> (std::istream& is, Chess& chess) {
  char c;
  for (int i = 0; i < 8; ++i) {
    for (int j = 0; j < 8; ++j) {
      is >> c;
      chess.board_get().add_piece(std::pair<char, char>( 'A' + j, '8' - i), c);
    }
  }
  is >> c;
  if (c == 'w') {
    chess.set_white(true);
  } else {
    chess.set_white(false);
  }
  return is;
}

